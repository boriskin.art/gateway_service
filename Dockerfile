# Build binary with downloaded go modules
FROM golang:1.16.7-buster as build

# Copy files
RUN mkdir /home/gateway_service
COPY ./go.mod /home/gateway_service
WORKDIR /home/gateway_service

# Download go modules to cache
RUN go mod download
COPY . /home/gateway_service

# Build application
RUN CGO_ENABLED=0 go build -o gateway_service ./cmd/gateway_service/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir gateway_service

# Copy files from build step
WORKDIR gateway_service
COPY --from=build /home/gateway_service/gateway_service .
RUN mkdir configs
COPY --from=build /home/gateway_service/config ./config

# Start app
CMD ["./gateway_service"]
