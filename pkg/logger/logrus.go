package logger

import "github.com/sirupsen/logrus"

func InitLogger() *logrus.Logger{
	log := logrus.New()
	log.SetLevel(logrus.DebugLevel)
	log.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
		DisableColors: false,
	})

	return log
}