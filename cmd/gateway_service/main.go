package main

import (
	"gateway_service/internal/gateway_service"
)

func main() {
	gateway_service.Run()
}