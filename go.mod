module gateway_service

go 1.16

require (
	github.com/imroc/req v0.3.0
	github.com/labstack/echo/v4 v4.5.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
)
