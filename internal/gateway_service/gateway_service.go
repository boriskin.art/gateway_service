package gateway_service

import (
	"gateway_service/config"
	"gateway_service/internal/delivery"
	"gateway_service/internal/usecase"
	"gateway_service/pkg/logger"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	"net/http"
)

func Run() {
	config.InitConfig()

	e := echo.New()

	// Middlewares
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time:${time_rfc3339_nano}, request_id=${id}, method=${method}, uri=${uri}, status=${status}, \n",
		CustomTimeFormat: "02-01-2006  15:04:05.00000",
	}))

	log := logger.InitLogger()
	gatewayUsecase := usecase.NewGatewayUsecase(log)
	delivery.NewGatewayHandler(e, gatewayUsecase)

	if err := e.Start(viper.GetString("port")); err != http.ErrServerClosed {
		log.Fatal(err)
	}

}