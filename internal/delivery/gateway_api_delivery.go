package delivery

import (
	"fmt"
	"gateway_service/internal/domain"
	"gateway_service/internal/usecase"
	"github.com/labstack/echo/v4"
)

type GatewayHandler struct {
	gatewayUsecase *usecase.GatewayUsecase
}

func NewGatewayHandler(e *echo.Echo, gatewayUsecase *usecase.GatewayUsecase) {
	handler := &GatewayHandler{gatewayUsecase: gatewayUsecase}

	e.POST("/sign-in", handler.SignIn)
	e.POST("/sign-up", handler.SignUp)
}

func (gh *GatewayHandler) SignIn(ctx echo.Context) error {
	var U struct {
		Email string `json:"email"`
		Password string `json:"password"`
	}
	if err := ctx.Bind(&U); err != nil {
		return err
	}

	fmt.Println(U)

	return nil
}

func (gh *GatewayHandler) SignUp(ctx echo.Context) error {
	user := domain.SignUpInput{}
	if err := ctx.Bind(&user); err != nil {
		return err
	}

	userID, err := gh.gatewayUsecase.SignUp(user)
	if err != nil {
		return err
	}
	ctx.String(200, userID)

	return nil
}