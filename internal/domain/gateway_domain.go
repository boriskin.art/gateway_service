package domain

type GatewayUsecase interface {
	SignUp() (string, error)
	SignIn() (string, error)
}
