package usecase

type SignInRequest struct {

}

type SignUpRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
}

type SignInResponse struct {
	StatusCode int         `json:"status_code"`
	Response   interface{} `json:"response"`
}

type SignUpResponse struct {
	StatusCode int         `json:"status_code"`
	Response   interface{} `json:"response"`
}