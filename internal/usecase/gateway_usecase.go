package usecase

import (
	"fmt"
	"gateway_service/internal/domain"
	"github.com/imroc/req"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type GatewayUsecase struct {
	r *req.Req
	Log *logrus.Logger
}

func NewGatewayUsecase(log *logrus.Logger) *GatewayUsecase {
	return &GatewayUsecase{
		r: req.New(),
		Log: log,
	}
}

func (gu *GatewayUsecase) SignUp(user domain.SignUpInput) (string, error) {
	// Set up post request to auth microservice
	response, err := gu.r.Post(
		"http://" + viper.GetString("auth_service_addr") + "/sign-up",
		req.BodyJSON(&SignUpRequest{
			Email: user.Email,
			Password: user.Password,
		},
	))
	if err != nil {
		return "", err
	}
	
	// Parse response to SignUpResponse
	signUpResponse := SignUpResponse{}
	err = response.ToJSON(&signUpResponse)
	if err != nil {
		return "", err
	}

	gu.Log.Info("1: ", signUpResponse)

	responseData, ok := signUpResponse.Response.(map[string]string)

	gu.Log.Info("2:", responseData)

	if signUpResponse.StatusCode != 0 {
		if !ok {
			return "", fmt.Errorf("error while parsing SignUp response from auth service")
		}
		return "", fmt.Errorf(responseData["error_message"])
	}

	return responseData["user_id"], nil
}

func (gu *GatewayUsecase) SignIn() (string, error) {
	return "", nil
}